<?php
/**
 * @file
 * Twitter Pull bean plugin.
 */

class ServiceLinksBean extends BeanPlugin {
  /**
   * Declares default block settings.
   */
  public function values() {
    return array(
      'service_links' => array(),
      'service_links_style' => SERVICE_LINKS_STYLE_IMAGE_AND_TEXT,
    );
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {
    // Get the service links selection form as provided by service_links.
    module_load_include('inc', 'service_links', 'service_links.admin');
    $form = service_links_admin_services();

    // service_links_admin_services uses system_settings_form to return the
    // form. We dont want to use those submit handlers and buttons, so we
    // remove them here.
    unset($form['#submit']);
    unset($form['actions']);
    
    // We need to set #tree in orfder for the settings to be saved to the bean.
    $form['service_links']['#tree'] = 1;

    // Fix up the default values
    // Use existing bean settings if they exist.
    $services = element_children($form['service_links']['service_links_show']);
    if (isset($bean->service_links['service_links_show']) && is_array($form['service_links']['service_links_show'])) {
      foreach ($services as $service) {
        $form['service_links']['service_links_show'][$service]['#default_value'] = $bean->service_links['service_links_show'][$service];
      }
    }
    else {
      foreach ($services as $service) {
        $form['service_links']['service_links_show'][$service]['#default_value'] = 0;
      }
    }

    // Style setting
    $form['how_to_show_the_links'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => t('How to display Service Links'),
    );
    $form['how_to_show_the_links']['service_links_style'] = array(
      '#type' => 'select',
      '#title' => t('Choose a style'),
      '#default_value' => $bean->service_links_style,
      '#options' => array(
        SERVICE_LINKS_STYLE_TEXT => t('Only Text'),
        SERVICE_LINKS_STYLE_IMAGE => t('Only Image'),
        SERVICE_LINKS_STYLE_IMAGE_AND_TEXT => t('Image and Text'),
      ),
    );

    $styles = module_invoke_all('sl_styles', FALSE);
    if (!empty($styles)) {
      $form['how_to_show_the_links']['service_links_style']['#options'] += $styles;
    }
    
    return $form;
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
    $content = array();
    $options = array();
    
    $options['short_links_use'] = variable_get('service_links_short_links_use', SERVICE_LINKS_SHORT_URL_USE_NEVER);

    $options['attributes'] = array('rel' => 'nofollow');
    if (variable_get('service_links_new_window', 0)) {
      $options['attributes'] += array('target' => '_blank');
    }
    $options['style'] = $bean->service_links_style;

    $options['link_weight'] = $bean->service_links['service_links_weight'];
    $options['link_show'] = array_filter($bean->service_links['service_links_show']);

    $options['text_to_append'] = strip_tags(variable_get('service_links_append_to_url', ''));

    $options['title_override'] = variable_get('service_links_override_title', SERVICE_LINKS_TAG_TITLE_NODE);
    $options['title_text'] = variable_get('service_links_override_title_text', '<title>');

    $options['link_to_front'] = FALSE;

    $content['service_links']['#markup'] = theme('service_links_block_format', array(
      'items' => service_links_render(NULL, FALSE, $options),
      'style' => $options['style'],
    ));

    return $content;
  }
}
